Greetings fellow members of the community;

First I would like to formally apologize for today's recent down time. While it was a very necessary event, it was still an inconvenience to you all. I'd like you all to know that this is the first step towards the future of our server. From here on out, we are going to be using a patch system that we are naming "The Ancients". More on that later. This is the initial patch, other wise known as TA0.1. Here is what has changed.

    Migrated server to a new location

    Up graded server equipment (8gb Virtual -> 24gb Dedicated xeon)

    Cleaned the world (Reset the world so that we can apply Patch TA1.0 on Monday)

    Other smaller improvements.

From here on out, we are going to announce new patches before they come out, allowing you to get ready for whats coming next. Later today I will be writing an announcement on Patch TA1.0 and explaining what direction we will be taking for the future.

Thanks- Maxwell